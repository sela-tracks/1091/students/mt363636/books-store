﻿using BooksAndJournals.Action;
using MyItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksAndJournals.Menu
{
    public static class RemoveMenu
    {
        public static void ShowRemoveSubMenu(DataBase database)
        {
            bool removeSubMenuOpen = true;
            int removeSubMenuSelectedIndex = 0;

            while (removeSubMenuOpen)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("=================");
                Console.WriteLine("REMOVE ITEMS MENU");
                Console.WriteLine("=================\n");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(Resource1.SelectAnOptionMessage);
                Console.WriteLine();
                string[] removeSubMenuOptions = {
               Resource1.RemoveMenuOption1,
               Resource1.RemoveMenuOption2,
               Resource1.RemoveMenuOption3,
            };

                for (int i = 0; i < removeSubMenuOptions.Length; i++)
                {
                    if (removeSubMenuSelectedIndex == i)
                    {
                        Console.ForegroundColor = ConsoleColor.White;
                        Console.WriteLine("    » " + removeSubMenuOptions[i]);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.WriteLine("      " + removeSubMenuOptions[i]);
                    }
                }

                Console.ResetColor();

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (removeSubMenuSelectedIndex > 0)
                            removeSubMenuSelectedIndex--;
                        break;
                    case ConsoleKey.DownArrow:
                        if (removeSubMenuSelectedIndex < 2)
                            removeSubMenuSelectedIndex++;
                        break;
                    case ConsoleKey.Enter:
                        if (removeSubMenuSelectedIndex == 0)
                        {
                            RemoveAction.RemoveBook(database); 
                        }
                        else if (removeSubMenuSelectedIndex == 1)
                        {
                            RemoveAction.RemoveJournal(database);
                        }
                        else if (removeSubMenuSelectedIndex == 2)
                        {
                            removeSubMenuOpen = false;
                        }
                        break;
                }
            }
        }
    }

}
