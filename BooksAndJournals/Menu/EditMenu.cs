﻿using BooksAndJournals.Action;
using MyItems;
using System;

namespace BooksAndJournals.Menu
{
    public static class EditMenu
    {
        public static void ShowEditSubMenu(DataBase database)
        {
            bool editSubMenuOpen = true;
            int editItemsSelectedIndex = 0;

            while (editSubMenuOpen)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine("===============");
                Console.WriteLine("EDIT ITEMS MENU");
                Console.WriteLine("===============\n");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(Resource1.SelectAnOptionMessage);
                Console.WriteLine();
                string[] editSubMenuOptions = {
                   Resource1.EditMenuOption1,
                   Resource1.EditMenuOption2,
                   Resource1.EditMenuOption3,


                };

                for (int i = 0; i < editSubMenuOptions.Length; i++)
                {
                    if (editItemsSelectedIndex == i)
                    {
                        Console.ForegroundColor = ConsoleColor.DarkGreen;
                        Console.WriteLine("    » " + editSubMenuOptions[i]);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.WriteLine("      " + editSubMenuOptions[i]);
                    }
                }

                Console.ResetColor();

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (editItemsSelectedIndex > 0)
                            editItemsSelectedIndex--;
                        break;
                    case ConsoleKey.DownArrow:
                        if (editItemsSelectedIndex < 2)
                            editItemsSelectedIndex++;
                        break;
                    case ConsoleKey.Enter:
                        if (editItemsSelectedIndex == 0)
                        {
                            EditAction editItemAction = new EditAction();
                            EditAction.EditBook();
                            editSubMenuOpen = false;
                        }
                        else if (editItemsSelectedIndex == 1)
                        {
                            EditAction editItemAction = new EditAction();
                            EditAction.EditJournal();
                            editSubMenuOpen = false;
                        }
                        else if (editItemsSelectedIndex == 2)
                        {
                            editSubMenuOpen = false;
                        }
                        break;
                }
            }
        }
    }
}
