﻿using BooksAndJournals.Action;
using MyItems;
using System;

namespace BooksAndJournals.Menu
{
    public class ViewMenu
    {
        public static void ShowViewSubMenu(DataBase database)
        {
            bool viewSubMenuOpen = true;
            int viewSubMenuSelectedIndex = 0;

            while (viewSubMenuOpen)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.DarkGreen;
                Console.WriteLine("===============");
                Console.WriteLine("VIEW ITEMS MENU");
                Console.WriteLine("===============\n");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(Resource1.SelectAnOptionMessage);

                string[] viewSubMenuOptions = {
                Resource1.ViewMenuOption1,
                Resource1.ViewMenuOption2,
                Resource1.ViewMenuOption3,
                Resource1.ViewMenuOptionDiscount,  
                Resource1.ViewMenuOption4          
              };

                for (int i = 0; i < viewSubMenuOptions.Length; i++)
                {
                    if (viewSubMenuSelectedIndex == i)
                    {
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.WriteLine("    » " + viewSubMenuOptions[i]);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.WriteLine("      " + viewSubMenuOptions[i]);
                    }
                }

                Console.ResetColor();

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (viewSubMenuSelectedIndex > 0)
                            viewSubMenuSelectedIndex--;
                        break;
                    case ConsoleKey.DownArrow:
                        if (viewSubMenuSelectedIndex < viewSubMenuOptions.Length - 1)  
                            viewSubMenuSelectedIndex++;
                        break;
                    case ConsoleKey.Enter:
                        if (viewSubMenuSelectedIndex == 0)
                        {
                            ViewAction viewItemAction = new ViewAction();
                            ViewAction.ViewBooks(database);
                        }
                        else if (viewSubMenuSelectedIndex == 1)
                        {
                            ViewAction viewItemAction = new ViewAction();
                            ViewAction.ViewJournals(database);
                        }
                        else if (viewSubMenuSelectedIndex == 2)
                        {
                            ViewAction viewItemAction = new ViewAction();
                            ViewAction.ViewAllItems(database);
                        }
                        else if (viewSubMenuSelectedIndex == 3)  
                        {
                            ViewAction viewItemAction = new ViewAction();
                            ViewAction.ViewBooksWithDiscount(database);
                        }
                        else if (viewSubMenuSelectedIndex == 4)  
                        {
                            viewSubMenuOpen = false;
                        }
                        break;
                }
            }
        }


    }
}
