﻿using BooksAndJournals.Action;
using MyItems;
using System;

namespace BooksAndJournals.Menu

{
    public static class AddMenu
    {
        public static void ShowAddItemsMenu(DataBase database)
        {
            bool addItemsMenuOpen = true;
            int addItemsSelectedIndex = 0;

            while (addItemsMenuOpen)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("==============");
                Console.WriteLine("ADD ITEMS MENU");
                Console.WriteLine("==============\n");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(Resource1.SelectAnOptionMessage);
                Console.WriteLine();
                string[] addItemsMenuOptions = {
               Resource1.AddMenuOption1,
               Resource1.AddMenuOption2,
               Resource1.AddMenuOption3,
            };

                for (int i = 0; i < addItemsMenuOptions.Length; i++)
                {
                    if (addItemsSelectedIndex == i)
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.WriteLine("    » " + addItemsMenuOptions[i]);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.WriteLine("      " + addItemsMenuOptions[i]);
                    }
                }

                Console.ResetColor();

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (addItemsSelectedIndex > 0)
                            addItemsSelectedIndex--;
                        break;
                    case ConsoleKey.DownArrow:
                        if (addItemsSelectedIndex < 2)
                            addItemsSelectedIndex++;
                        break;
                    case ConsoleKey.Enter:
                        if (addItemsSelectedIndex == 0)
                        {
                            AddAction.AddBook(database);
                            addItemsMenuOpen = false;
                        }
                        else if (addItemsSelectedIndex == 1)
                        {
                            AddAction.AddJournal(database);
                            addItemsMenuOpen = false;
                        }
                        else if (addItemsSelectedIndex == 2)
                        {
                            addItemsMenuOpen = false;
                        }
                        break;
                }
            }
        }
    }

}
