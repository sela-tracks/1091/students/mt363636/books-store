﻿using MyItems;
using System;
using System.Threading;

namespace BooksAndJournals.Menu
{
    public static class MainMenu
    {
        public static void ShowMainMenu(DataBase dataBase)
        {
            bool mainMenuOpen = true;
            int mainMenuSelectedIndex = 0;

            while (mainMenuOpen)
            {
                Console.Clear();
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("\r\n______             _      _____ _____              \r\n| ___ \\           | |    /  ___|_   _|             \r\n| |_/ / ___   ___ | | __ \\ `--.  | | ___  _ __ ___ \r\n| ___ \\/ _ \\ / _ \\| |/ /  `--. \\ | |/ _ \\| '__/ _ \\\r\n| |_/ / (_) | (_) |   <  /\\__/ / | | (_) | | |  __/\r\n\\____/ \\___/ \\___/|_|\\_\\ \\____/  \\_/\\___/|_|  \\___|\r\n                                                   \r\n                                                   \r\n,         ,\r\n|\\\\\\\\ ////|\r\n| \\\\\\V/// |\r\n|  |~~~|  |\r\n|  |===|  |\r\n|  |j  |  |\r\n|  | g |  |\r\n \\ |  s| /\r\n  \\|===|/\r\n   '---'");
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine(Resource1.MainMenuWelcomeMessage);
                Console.WriteLine();
                Console.WriteLine(Resource1.SelectAnOptionMessage);
                Console.WriteLine();
                string[] mainMenuOptions = {
                    Resource1.MenuOption1,
                    Resource1.MenuOption2,
                    Resource1.MenuOption3,
                    Resource1.MenuOption4,
                    Resource1.MenuOption5

            };

                for (int i = 0; i < mainMenuOptions.Length; i++)
                {
                    if (mainMenuSelectedIndex == i)
                    {
                        Console.ForegroundColor = ConsoleColor.DarkYellow;
                        Console.WriteLine("    » " + mainMenuOptions[i]);
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.DarkCyan;
                        Console.WriteLine("      " + mainMenuOptions[i]);
                    }
                }

                Console.ResetColor();

                ConsoleKeyInfo keyInfo = Console.ReadKey(true);
                switch (keyInfo.Key)
                {
                    case ConsoleKey.UpArrow:
                        if (mainMenuSelectedIndex > 0)
                            mainMenuSelectedIndex--;
                        break;
                    case ConsoleKey.DownArrow:
                        if (mainMenuSelectedIndex < 4)
                            mainMenuSelectedIndex++;
                        break;
                    case ConsoleKey.Enter:
                        if (mainMenuSelectedIndex == 0)
                        {
                            ViewMenu.ShowViewSubMenu(dataBase);
                        }
                        else if (mainMenuSelectedIndex == 1)
                        {
                            AddMenu.ShowAddItemsMenu(dataBase);
                        }
                        else if (mainMenuSelectedIndex == 2)
                        {
                            RemoveMenu.ShowRemoveSubMenu(dataBase);
                        }
                        else if (mainMenuSelectedIndex == 3)
                        {
                            EditMenu.ShowEditSubMenu(dataBase);
                        }
                        else if (mainMenuSelectedIndex == 4)
                        {
                            Console.WriteLine("Exiting and Saving Data...");
                            mainMenuOpen = false;
                           
                        }
                        break;
                }
            }
        }
    }

}
