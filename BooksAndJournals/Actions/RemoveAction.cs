﻿using MyItems;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BooksAndJournals.Action
{
    public class RemoveAction
    {
        private static DataBase dataBase = new DataBase();

        public static void RemoveBook(DataBase dataBase)
        {
            try
            {
                Console.Write("Enter ISBN of the book to remove: ");
                string isbn = Console.ReadLine();
              
                Book bookToRemove = dataBase.FindBookByISBN(isbn);

                if (bookToRemove != null)
                {
                    Console.WriteLine($"Are you sure you want to remove the book '{bookToRemove.ItemName}'? (Y/N)");
                    char confirmation = Console.ReadKey().KeyChar;

                    if (confirmation == 'Y' || confirmation == 'y')
                    {
                        DataBase.RemoveItem(bookToRemove);
                        string folder = "books";
                        string fileName = Path.Combine(folder, $"{isbn}.txt");
                        LoadAndSaveData.SaveDataInFile(DataBase.items);
                        Console.WriteLine(Resource1.Waiting);
                        Thread.Sleep(2000);

                        Console.WriteLine("\nBook removed successfully.");
                    }
                    else
                    {
                        Console.WriteLine("\nRemoval operation cancelled.");
                    }
                }
                else
                {
                    Console.WriteLine(Resource1.BookNotFound);
                }

                Console.WriteLine(Resource1.PressAnyKey);
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
        }

        public static void RemoveJournal(DataBase database)
        {
            try
            {
                Console.Write("Enter ISBN of the journal to remove: ");
                string isbn = Console.ReadLine();
               
                Journal journalToRemove = database.FindJournalByISBN(isbn);

                if (journalToRemove != null)
                {
                    Console.WriteLine($"Are you sure you want to remove the journal '{journalToRemove.ItemName}'? (Y/N)");
                    char confirmation = Console.ReadKey().KeyChar;

                    if (confirmation == 'Y' || confirmation == 'y')
                    {
                        DataBase.RemoveItem(journalToRemove);
                        string folder = "journals";
                        string fileName = Path.Combine(folder, $"{isbn}.txt");
                        LoadAndSaveData.SaveDataInFile(DataBase.items);
                        Console.WriteLine(Resource1.Waiting);
                        Thread.Sleep(2000);
                        Console.WriteLine("\nJournal removed successfully.");
                    }
                    else
                    {
                        Console.WriteLine("\nRemoval operation cancelled.");
                    }
                }
                else
                {
                    Console.WriteLine(Resource1.JournalNotFound);
                }

                Console.WriteLine(Resource1.PressAnyKey);
                Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"An error occurred: {ex.Message}");
            }
        }

    }
}
