﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MyItems;

namespace BooksAndJournals.Action
{
    public class ViewAction
    {
       
        public static void ViewBooks(DataBase database)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("\r\nBooks: \n");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            foreach (AbstractItem item in database)
            {
                if (item is Book book)
                {
                    Console.WriteLine(book);
                }
            }
            Thread.Sleep(2000);
            Console.WriteLine("\nPress any key to return to the previous menu...");
            Console.ReadKey(true);
        }

        public static void ViewJournals(DataBase database)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("\r\nJournals: \n");
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine();
            foreach (AbstractItem item in database)
            {
                if (item is Journal journal)
                {
                    Console.WriteLine(journal);
                }
            }
            Thread.Sleep(2000);
            Console.WriteLine("\nPress any key to return to the previous menu...");
            Console.ReadKey(true);
        }

        public static void ViewBooksWithDiscount(DataBase database)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("\r\nViewing Books with Discount\r\n");
            Console.ForegroundColor = ConsoleColor.White;
            foreach (AbstractItem item in database)
            {
                if (item is Book book && book.Discount > 0)
                {
                    Console.WriteLine(book);
                }
            }
            Thread.Sleep(2000);
            Console.WriteLine("\nPress any key to return to the main menu...");
            Console.ReadKey(true);
        }

        public static void ViewAllItems(DataBase database)
        {
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.DarkYellow;
            Console.WriteLine("\r\nAll Items:");
            Console.ForegroundColor = ConsoleColor.White;
            foreach (AbstractItem item in database)
            {
                Console.WriteLine(item);
            }
            Thread.Sleep(2000);
            Console.WriteLine("\nPress any key to return to the previous menu...");
            Console.ReadKey(true);
        }
    }

}
