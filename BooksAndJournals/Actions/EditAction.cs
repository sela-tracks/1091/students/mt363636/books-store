﻿using MyItems;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksAndJournals.Action
{
    public class EditAction
    {
        private static DataBase DataBase = new DataBase();

        public static void EditBook()
        {
            Console.Write("Enter ISBN of the book to edit: ");
            string isbn = Console.ReadLine();

            Book bookToEdit = DataBase.OfType<Book>().FirstOrDefault(book => book.ISBN == isbn);
            if (bookToEdit != null)
            {
                UpdateBookProperties(bookToEdit);
            }
            else
            {
                Console.WriteLine(Resource1.BookNotFound);
                Console.WriteLine(Resource1.PressAnyKey);
                Console.ReadKey();
                return; 
            }

            Console.WriteLine("Press 'E' to edit another book or any other key to exit to the submenu:");
            if (Console.ReadKey().Key == ConsoleKey.E)
            {
                EditBook(); 
            }

            Console.ReadKey();
        }

        public static void EditJournal()
        {
            Console.Write("Enter ISBN of the journal to edit: ");
            string isbn = Console.ReadLine();

            Journal journalToEdit = DataBase.OfType<Journal>().FirstOrDefault(journal => journal.ISBN == isbn);
            if (journalToEdit != null)
            {
                UpdateJournalProperties(journalToEdit);
            }
            else
            {
                Console.WriteLine(Resource1.JournalNotFound);
                Console.WriteLine(Resource1.PressAnyKey);
                Console.ReadKey();
                return; 
            }

            Console.WriteLine("Press 'E' to edit another journal or any other key to exit to the main menu:");
            if (Console.ReadKey().Key == ConsoleKey.E)
            {
                EditJournal(); 
            }

            Console.ReadKey();
        }


        private static void UpdateBookProperties(Book book)
        {
            Console.WriteLine($"Editing Book with ISBN: {book.ISBN}");

            book.ItemName = ReadInputWithSkip(Resource1.ItemNameInpt, book.ItemName);
            book.PrintDate = ReadDateInputWithSkip(Resource1.PrintDate, book.PrintDate);
            book.Edition = ReadInputWithSkip(Resource1.EditionInpt, book.Edition);
            book.CopyNum = ReadPositiveIntInputWithSkip(Resource1.CopyNumInpt, book.CopyNum);
            book.Summary = ReadInputWithSkip(Resource1.SummaryInpt, book.Summary);

            Console.WriteLine(Resource1.GenreInpt);
            foreach (GenreType genre in Enum.GetValues(typeof(GenreType)))
            {
                Console.WriteLine($"{(int)genre}. {genre}");
            }

            int genreType;
            if (int.TryParse(Console.ReadLine(), out genreType) && Enum.IsDefined(typeof(GenreType), genreType))
            {
                book.Genre = (GenreType)genreType;
            }

            double discount;
            Console.Write(Resource1.DiscountInpt);
            if (double.TryParse(Console.ReadLine(), out discount) && discount >= 0 && discount <= 100)
            {
                book.Discount = discount;
            }

            book.Publisher = ReadInputWithSkip(Resource1.PublisherInpt, book.Publisher);

            Console.WriteLine(Resource1.BookPropertiesUpdated);
            Console.ReadKey(true);
        }


        private static void UpdateJournalProperties(Journal journal)
        {
            Console.WriteLine($"Editing Journal with ISBN: {journal.ISBN}");

            journal.ItemName = ReadInputWithSkip(Resource1.ItemNameInpt, journal.ItemName);
            journal.PrintDate = ReadDateInputWithSkip(Resource1.PrintDate, journal.PrintDate);
            journal.Edition = ReadInputWithSkip(Resource1.EditionInpt, journal.Edition);
            journal.CopyNum = ReadPositiveIntInputWithSkip(Resource1.CopyNumInpt, journal.CopyNum);
            journal.JournalType = ReadInputWithSkip(Resource1.JournalTypeInpt, journal.JournalType);

            Console.WriteLine(Resource1.JournalPropertiesUpdated);
            Console.ReadKey(true);
        }

        private static string ReadInputWithSkip(string prompt, string currentValue)
        {
            Console.Write($"{prompt} ({currentValue}): ");
            string input = Console.ReadLine();
            return string.IsNullOrWhiteSpace(input) ? currentValue : input;
        }

        private static DateTime ReadDateInputWithSkip(string prompt, DateTime currentValue)
        {
            Console.Write($"{prompt} ({currentValue.ToString("MM/dd/yyyy")}): ");
            DateTime inputDate;
            if (DateTime.TryParse(Console.ReadLine(), out inputDate))
            {
                return inputDate;
            }
            return currentValue;
        }

        private static int ReadPositiveIntInputWithSkip(string prompt, int currentValue)
        {
            Console.Write($"{prompt} ({currentValue}): ");
            int inputInt;
            if (int.TryParse(Console.ReadLine(), out inputInt) && inputInt > 0)
            {
                return inputInt;
            }
            return currentValue;
        }



    }
}
    

