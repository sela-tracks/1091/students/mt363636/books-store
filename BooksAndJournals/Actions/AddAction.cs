﻿using MyItems;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BooksAndJournals.Action
{
    public class AddAction
    {
        
        private static GenreType genreType;

        public static void AddBook(DataBase dataBase)
        {
            string isbn, itemName, edition, summary, publisher;
            DateTime printDate;
            int copyNum;
            GenreType selectedGenre;

            double discount;

            Console.Write(Resource1.ISBNinpt);
            isbn = Console.ReadLine();
            while (!Validator.IsValidISBN(isbn))
            {
                Console.WriteLine(Resource1.InvalidIsbn);
                Console.Write(Resource1.ISBNinpt);
                isbn = Console.ReadLine();
            }

            Console.Write(Resource1.ItemNameInpt);
            itemName = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(itemName))
            {
                Console.WriteLine(Resource1.NameIsEmpty);
                Console.Write(Resource1.ItemNameInpt);
                itemName = Console.ReadLine();
            }

            Console.Write(Resource1.PrintDate);
            while (!DateTime.TryParse(Console.ReadLine(), out printDate))
            {
                Console.WriteLine(Resource1.InvalidDate);
                Console.Write(Resource1.PrintDate);
            }

            Console.Write(Resource1.EditionInpt);
            edition = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(edition))
            {
                Console.WriteLine(Resource1.EditionIsEmpty);
                Console.Write(Resource1.EditionInpt);
                edition = Console.ReadLine();
            }

            Console.Write(Resource1.CopyNumInpt);
            while (!int.TryParse(Console.ReadLine(), out copyNum) || copyNum <= 0)
            {
                Console.WriteLine(Resource1.invalidCopyNum);
                Console.Write(Resource1.CopyNumInpt);
            }

            Console.Write(Resource1.SummaryInpt);
            summary = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(summary))
            {
                Console.WriteLine(Resource1.SummaryIsEmpty);
                Console.Write(Resource1.SummaryInpt);
                summary = Console.ReadLine();
            }

            Console.WriteLine(Resource1.GenreInpt);
            foreach (GenreType genre in Enum.GetValues(typeof(GenreType)))
            {
                Console.WriteLine($"{(int)genre + 1}. {genre}");
            }

            int genreSelection;
            while (!int.TryParse(Console.ReadLine(), out genreSelection) || genreSelection < 1 || genreSelection > 12)
            {
                Console.WriteLine(Resource1.InvalidGenre);
                Console.Write(Resource1.GenreInpt);
            }
            selectedGenre = (GenreType)(genreSelection - 1);


            Console.Write(Resource1.DiscountInpt);
            while (!double.TryParse(Console.ReadLine(), out discount) || discount < 0 || discount > 100)
            {
                Console.WriteLine(Resource1.InvalidDiscount);
                Console.Write(Resource1.DiscountInpt);
            }

            Console.Write(Resource1.PublisherInpt);
            publisher = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(publisher))
            {
                Console.WriteLine(Resource1.PublisherIsEmpty);
                Console.Write(Resource1.PublisherInpt);
                publisher = Console.ReadLine();
            }
            Book newBook = new Book(isbn, itemName, printDate, edition, copyNum, summary, selectedGenre, discount, publisher);
       

            Console.WriteLine(Resource1.BookAdded);
            Console.WriteLine(Resource1.AllParams);
            Console.WriteLine(newBook);


            Console.WriteLine(Resource1.PressYtoConfirmorOtherToCancel);

            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.WriteLine(Resource1.Waiting);
                Thread.Sleep(4000);
                DataBase.AddItem(newBook);
                LoadAndSaveData.SaveDataInFile(DataBase.items);
                Console.WriteLine(Resource1.BookSuccessful);
            }
            else
            {
                Console.WriteLine(Resource1.Waiting);
                Thread.Sleep(4000);
                Console.WriteLine(Resource1.BookCanceled);
            }
        }


        public static void AddJournal(DataBase database)
        {
            string isbn, itemName, edition, journalType;
            DateTime printDate;
            int copyNum;

            Console.Write(Resource1.ISBNinpt);
            isbn = Console.ReadLine();
            while (!Validator.IsValidISBN(isbn))
            {
                Console.WriteLine(Resource1.InvalidIsbn);
                Console.Write(Resource1.ISBNinpt);
                isbn = Console.ReadLine();
            }

            Console.Write(Resource1.ItemNameInpt);
            itemName = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(itemName))
            {
                Console.WriteLine(Resource1.NameIsEmpty);
                Console.Write(Resource1.ItemNameInpt);
                itemName = Console.ReadLine();
            }

            Console.Write(Resource1.PrintDate);
            while (!DateTime.TryParse(Console.ReadLine(), out printDate))
            {
                Console.WriteLine(Resource1.InvalidDate);
                Console.Write(Resource1.PrintDate);
            }

            Console.Write(Resource1.EditionInpt);
            edition = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(edition))
            {
                Console.WriteLine(Resource1.EditionIsEmpty);
                Console.Write(Resource1.EditionInpt);
                edition = Console.ReadLine();
            }

            Console.Write(Resource1.CopyNumInpt);
            while (!int.TryParse(Console.ReadLine(), out copyNum) || copyNum <= 0)
            {
                Console.WriteLine(Resource1.invalidCopyNum);
                Console.Write(Resource1.CopyNumInpt);
            }

            Console.Write(Resource1.JournalTypeInpt);
            journalType = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(journalType))
            {
                Console.WriteLine(Resource1.JournalTypeIsEmpty);
                Console.Write(Resource1.JournalTypeInpt);
                journalType = Console.ReadLine();
            }

           
            Journal newJournal = new Journal(isbn, itemName, printDate, edition, copyNum, journalType);
            Console.WriteLine(Resource1.AllParams);
            Console.WriteLine(newJournal);

            Console.WriteLine(Resource1.PressYtoConfirmorOtherToCancel);
           
            if (Console.ReadKey().Key == ConsoleKey.Y)
            {
                Console.WriteLine(Resource1.Waiting);
                Thread.Sleep(4000);
                DataBase.AddItem(newJournal);
                LoadAndSaveData.SaveDataInFile(DataBase.items);
                Console.WriteLine(Resource1.JournalSuccessfull);
            }
            else
            {
                Console.WriteLine(Resource1.Waiting);
                Thread.Sleep(4000);
                Console.WriteLine(Resource1.JournalCancelled);
            }
        }

    }
}
