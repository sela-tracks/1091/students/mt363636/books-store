﻿
using BooksAndJournals;
using BooksAndJournals.Menu;
using MyItems;
using System;

namespace ProjectBooksAndJournals
{
    public class Program
    {

        public static DataBase dataBase = new DataBase();

        static void Main(string[] args)
        {
            bool loggedIn = LoginManager.PerformLogin();
            if (loggedIn)
            {
                MainMenu.ShowMainMenu(dataBase);
                dataBase.CreateCommonDoc();
            }
        }



    }
}
