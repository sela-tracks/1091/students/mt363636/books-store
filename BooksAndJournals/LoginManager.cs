﻿using MyItems;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksAndJournals
{
    public class LoginManager
    {
        public static bool PerformLogin()
        {
            Console.ForegroundColor = ConsoleColor.Green;

            while (true) 
            {
                Console.Clear(); 
                Console.WriteLine(Resource1.WelcomeLoginMessage);
                Console.WriteLine(Resource1.UmustSignInMessage);
                Console.WriteLine(Resource1.Username_Password);
                

                Console.Write("Username: ");
                string username = Console.ReadLine();

                Console.Write("Password: ");
                string password = Console.ReadLine();

                try
                {
                    if (Validator.ValidateLogin(username, password))
                    {
                        Console.WriteLine("Login successful!\n");
                        return true; 
                    }
                    else
                    {
                        Console.WriteLine(Resource1.InvalidLogin);
                        Console.WriteLine("Press any key to continue...");
                        Console.ReadKey();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"An error occurred: {ex.Message}\n");
                }
            }
        }
    }


}
