﻿using System;
using System.Globalization;
using System.Text.RegularExpressions;

namespace MyItems
{
    public class Validator
    {

        public static bool ValidateLogin(string username, string password)
        {
            string[] validUsernames = { "Martin", "Admin" };
            string[] validPasswords = { "12345", "Aa123" };

            for (int i = 0; i < validUsernames.Length; i++)
            {
                if (username == validUsernames[i] && password == validPasswords[i])
                {
                    return true;
                }
            }

            return false;
        }
        public static bool IsValidISBN(string isbn)
        {

            return Regex.IsMatch(isbn, @"^(?:\d{13}|\d{9}[\dXx])$");
        }

        public static bool IsValidDate(string dateStr)
        {
            string[] validFormats = { "MM/dd/yyyy", "MM.dd.yyyy" };

            foreach (string format in validFormats)
            {
                if (DateTime.TryParseExact(dateStr, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out _))
                {
                    return true;
                }
            }

            return false;
        }


        public static bool IsValidEdition(string edition)
        {

            return !string.IsNullOrWhiteSpace(edition);
        }

        public static bool IsValidCopyNumber(int copyNum)
        {

            return copyNum > 0;
        }

        public static bool IsValidSummary(string summary)
        {

            return !string.IsNullOrWhiteSpace(summary);
        }

        public static bool IsValidGenreType(int genreType)
        {
            return Enum.IsDefined(typeof(GenreType), genreType);
        }

        public static bool IsValidDiscount(double discount)
        {
            return discount >= 0 && discount <= 100;
        }

        public static bool IsValidPublisher(string publisher)
        {
            return !string.IsNullOrWhiteSpace(publisher);
        }


        public static bool IsValidBook(Book book)
        {
            if (book == null)
                return false;

            if (!IsValidISBN(book.ISBN))
                return false;

            if (!IsValidEdition(book.Edition))
                return false;

            if (string.IsNullOrWhiteSpace(book.ItemName))
                return false;


            if (book.CopyNum <= 0)
                return false;

            if (book.Discount < 0 || book.Discount > 100)
                return false;

            if (string.IsNullOrWhiteSpace(book.Publisher))
                return false;

            return true;
        }

        public static bool IsValidJournal(Journal journal)
        {
            if (journal == null)
                return false;

            if (!IsValidISBN(journal.ISBN))
                return false;

            if (!IsValidEdition(journal.Edition))
                return false;

            if (string.IsNullOrWhiteSpace(journal.ItemName))
                return false;

            if (journal.PrintDate == DateTime.Now)
                return false;

            if (journal.CopyNum <= 0)
                return false;

            if (string.IsNullOrWhiteSpace(journal.JournalType))
                return false;

            return true;
        }


    }
}
