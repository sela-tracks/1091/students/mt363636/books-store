﻿using System;

namespace MyItems
{
    public class Journal :AbstractItem
    {
        public string JournalType { get; set; }

        public Journal(string isbn, string itemName, DateTime printDate, string edition, int copyNum, string journalType)
            : base(isbn, itemName, printDate, edition, copyNum)
        {
            JournalType = journalType;
        }

        public override string ToString()
        {
            return base.ToString() +
                   $"Journal Type: {JournalType}\r\n" +
                   "------------------------\r\n";
        }
    }
}
