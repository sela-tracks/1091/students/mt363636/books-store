﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyItems
{
    public enum GenreType
    {
        Sci_Fi,
        Mystery,
        Fantasy,
        Thriller,
        Adventure,
        Romance,
        Novel,
        Biography,
        History,
        Geography,
        Geo_Politics,
        True_Story,
        Poetry

    }
}
