﻿using Aspose.Words;
using Aspose.Words.Tables;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Reflection;

namespace MyItems
{
    public class DataBase : IEnumerable<AbstractItem>
    {
        public static List<AbstractItem> items = new List<AbstractItem>();

        public DataBase()
        {
            DataBase.LoadData();
        }


        public static void AddItem(AbstractItem item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item), "Item cannot be null.");

            items.Add(item);

            LoadAndSaveData.SaveDataInFile(items);
        }


        public static bool RemoveItem(AbstractItem item)
        {
            bool removed = items.Remove(item);

            if (removed)
            {
                LoadAndSaveData.SaveDataInFile(items);
            }

            return removed;
        }

        public static void LoadData()
        {
            items = LoadAndSaveData.LoadDataFromFile();
        }

        public IEnumerator<AbstractItem> GetEnumerator()
        {
            return items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Book FindBookByISBN(string isbn)
        {
            return items.OfType<Book>().FirstOrDefault(book => book.ISBN == isbn);
        }


        public Journal FindJournalByISBN(string isbn)
        {
            return items.OfType<Journal>().FirstOrDefault(journal => journal.ISBN == isbn);
        }

        public void CreateCommonDoc()
        {
            Document doc = new Document();
            DocumentBuilder builder = new DocumentBuilder(doc);

            ConfigureDocumentProperties(builder);

            // Save the document as a Word file
            string outputPath = "AllItemsDocument.docx";

            // Insert Books Section
            InsertSectionTitle(builder, "Books");
            InsertImage(builder, "book1.png");
            InsertItems(builder, typeof(Book));

            // Insert Journals Section
            InsertSectionTitle(builder, "Journals");
            InsertImage(builder, "Journal1.png");
            InsertItems(builder, typeof(Journal));

            // Save the document
            doc.Save(outputPath);

            Console.WriteLine("Common Word document saved successfully.");
            Console.WriteLine("Goodbye, see you soon! :)");
        }

        private void ConfigureDocumentProperties(DocumentBuilder builder)
        {
            builder.ParagraphFormat.StyleName = "Heading 3";
            builder.Font.Size = 14;
            builder.Font.Name = "Calibri";
            builder.Font.ClearFormatting();
        }

        private void InsertSectionTitle(DocumentBuilder builder, string title)
        {
            builder.Font.Size = 16;
            builder.Font.Bold = true;
            builder.Writeln(title);
            builder.Font.ClearFormatting();
        }

        private void InsertItems(DocumentBuilder builder, Type itemType)
        {
            foreach (var item in items)
            {
                if (itemType.IsInstanceOfType(item))
                {
                    InsertItemDetails(builder, item, itemType);
                    builder.Writeln($"Summary: {item}");
                    builder.Writeln(new string('-', 80));
                }
            }
        }

        private void InsertImage(DocumentBuilder builder, string imageName)
        {
            builder.InsertImage(imageName);
            builder.Writeln();
        }


        private void InsertItemDetails(DocumentBuilder builder, object item, Type itemType)
        {
            builder.Font.Size = 12;
            builder.Font.Bold = false;
            builder.ParagraphFormat.StyleName = "Normal";
            builder.Writeln();

            builder.Font.Color = Color.Black;

            Table table = builder.StartTable();

            // Set the preferred width to "Auto" for the first column (Item Type)
            builder.InsertCell();
            builder.CellFormat.PreferredWidth = PreferredWidth.Auto;
            builder.Font.Bold = true;
            builder.Write("Item Type:");
            builder.Font.Bold = false;
            builder.Write((item is Book) ? "Book" : "Journal");

            // Set the preferred width to "Auto" for the second column (Title)
            builder.InsertCell();
            builder.CellFormat.PreferredWidth = PreferredWidth.Auto;
            builder.Font.Bold = true;
            builder.Write("Title:");
            builder.Font.Bold = false;
            builder.Write(GetItemTitle(item));

            builder.EndRow();

            if (itemType == typeof(Book))
            {
                InsertBookDetails(builder, (Book)item);
            }
            else if (itemType == typeof(Journal))
            {
                InsertJournalDetails(builder, (Journal)item);
            }

            builder.EndTable();
        }



        private string GetItemTitle(object item)
        {
            PropertyInfo titleProperty = item.GetType().GetProperty("ItemName");
            return (string)titleProperty.GetValue(item);
        }

        private void InsertBookDetails(DocumentBuilder builder, Book book)
        {
            InsertTableCell(builder, "Author/Publisher:", book.Publisher);
            InsertTableCell(builder, "Publication Date:", book.PrintDate.ToString("yyyy-MM-dd"));
            InsertTableCell(builder, "Edition:", book.Edition);
            InsertTableCell(builder, "Copy Number:", book.CopyNum.ToString());

            // Format the discount as a decimal (0.00) and convert it to a string
            string formattedDiscount = book.Discount.ToString("0.00");

            InsertTableCell(builder, "Discount:", $"{formattedDiscount}%");
        }

        private void InsertJournalDetails(DocumentBuilder builder, Journal journal)
        {
            InsertTableCell(builder, "Journal Type:", journal.JournalType);
            InsertTableCell(builder, "Publication Date:", journal.PrintDate.ToString("yyyy-MM-dd"));
        }

        private void InsertTableCell(DocumentBuilder builder, string label, string value)
        {
            builder.InsertCell();
            builder.Font.Bold = true;
            builder.Write(label);
            builder.Font.Bold = false;
            builder.Write(value);
        }



















    }

}
