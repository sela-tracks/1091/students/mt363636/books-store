﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System;
using System.IO;



namespace MyItems
{
    public class LoadAndSaveData
    {
        static string pathToFile = "items.txt";
       
        static JsonSerializerSettings settings = new JsonSerializerSettings()
        {
            TypeNameHandling = TypeNameHandling.All
        };

        public  static void SaveDataInFile(List<AbstractItem> data)
        {
            string content = JsonConvert.SerializeObject(data, settings);
            File.WriteAllText(pathToFile, content);
        }

       public  static List<AbstractItem> LoadDataFromFile()
        {
            if (File.Exists(pathToFile)) {

                string dataFromFile = File.ReadAllText(pathToFile);
                Object data = JsonConvert.DeserializeObject(dataFromFile, settings);

                List<AbstractItem> items = data as List<AbstractItem>;
                return items;
            }
          

            return new List<AbstractItem>();
        }

    
    }

}
