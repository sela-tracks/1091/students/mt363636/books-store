﻿using System;

namespace MyItems
{
    public class Book : AbstractItem
    {
        public string Summary { get; set; }
        public GenreType Genre { get; set; }
        public double Discount { get; set; }
        public string Publisher { get; set; }

        public Book(string isbn, string itemName, DateTime printDate, string edition, int copyNum, string summary, GenreType genre, double discount, string publisher)
            : base(isbn, itemName, printDate, edition, copyNum)
        {
            Summary = summary;
            Genre = genre;
            Discount = discount;
            Publisher = publisher;
        }

        public override string ToString()
        {
            return base.ToString() +
                   $"Summary: {Summary}\r\n" +
                   $"Genre: {Genre}\r\n" +
                   $"Discount: {Discount}%\r\n" +
                   $"Publisher: {Publisher}\r\n" +
                   "------------------------\r\n";
        }

    }
}
