﻿using System;
using System.Runtime.InteropServices;

namespace MyItems
{
    public abstract class AbstractItem 
    {
        public string ISBN { get; set; }
        public string ItemName { get; set; }
        public DateTime PrintDate { get; set; }
        public string Edition { get; set; }
        public int CopyNum { get; set; }

        public AbstractItem(string isbn, string itemName, DateTime printDate, string edition, int copyNum)
        {
            ISBN = isbn;
            ItemName = itemName;
            PrintDate = printDate;
            Edition = edition;
            CopyNum = copyNum;
        }

        public override string ToString()
        {
            return $"Item Type: {GetType().Name}\r\n" +
                   $"ISBN: {ISBN}\r\n" +
                   $"Item Name: {ItemName}\r\n" +
                   $"Print Date: {PrintDate}\r\n" +
                   $"Edition: {Edition}\r\n" +
                   $"Copy Number: {CopyNum}\r\n";
        }


    }

    
}
